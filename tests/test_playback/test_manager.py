import json
import os
import time
from shutil import copyfile

from pymp.__main__ import Core
from pymp.db import ConfigInfo, Song, Playlist, PlayListEntry, PlayInfo, SongLink
from tests.test_db.base import BaseCleanOnEach


class TestPlaybackManager(BaseCleanOnEach):
    def setUp(self):
        super(TestPlaybackManager, self).setUp()
        self.core = Core()
        config_info = ConfigInfo.get_or_create(name="lib_handler.file_system")
        path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), "test_library")
        config_info.update(data_blob=json.dumps({"monitored_dirs": path}))
        self.core._library_handlers[0].rescan()
        self.core._library_handlers[0].stop_process()
        song = Song.get_one(pk=2)
        self.assertEqual(('TestArtist', "TestAlbum", "TestTitle"), (song.artist, song.album, song.title))
        self.core.playback_manager.start()
        self.core.playback_manager._run_command("set_repeat_mode", "repeat-all")

    def test_fails_on_wrong_command(self):
        with self.assertRaisesRegex(Exception, "'PlayBackThread' object has no attribute '_command_unknown_command'"):
            self.core.playback_manager._run_command("unknown_command")

    def test_play_pause(self):
        self.core.playback_manager._run_command('stop')
        self.core.playback_manager._run_command('play_pause')
        self.assertEqual('playing', self.core.playback_manager._get_status()['status'])
        self.assertEqual(5, self.core.playback_manager._get_status()['duration'])

        self.core.playback_manager._run_command('play_pause')
        self.assertEqual('paused', self.core.playback_manager._get_status()['status'])

        self.core.playback_manager._run_command('play_pause')
        self.assertEqual('playing', self.core.playback_manager._get_status()['status'])

    def _setup_playlist(self):
        self.playlist = Playlist.create(name="Test")
        for index in range(5):
            PlayListEntry.create(playlist_id=self.playlist.pk, song_id=2)
        self.core.playback_manager._run_command('change_playlist', 1)
        status = self.core.playback_manager._get_status()
        self.assertEqual({'duration': 0,
                          'front_queue': [],
                          'in_playlist_pk': 0,
                          'playlist_pk': 1,
                          'playlist_query': None,
                          'position': 0,
                          'repeat_mode': 'repeat-all',
                          'song': None,
                          'song_pk': 0,
                          'status': 'stopped',
                          'volume': 1}, status)

    def test_play_next(self):
        self._setup_playlist()
        self.core.playback_manager._run_command('play_next')
        status = self.core.playback_manager._get_status()
        self.assertEqual('playing', status['status'])
        self.assertEqual(1, status['in_playlist_pk'])

        self.core.playback_manager._run_command('play_next')
        status = self.core.playback_manager._get_status()
        self.assertEqual('playing', status['status'])
        self.assertEqual(2, status['in_playlist_pk'])

    def test_play_next_at_playback_end(self):
        song = Song.get_one(pk=2)
        self.assertEqual(0, song.listened_time)
        self._setup_playlist()
        self.core.playback_manager._run_command('play', 1)
        status = self.core.playback_manager._get_status()
        self.assertEqual('playing', status['status'])
        self.assertEqual(1, status['in_playlist_pk'])
        for i in range(10):
            time.sleep(1)
            status = self.core.playback_manager._get_status()
            self.assertEqual('playing', status['status'])
            if 2 == status['in_playlist_pk']:
                play_infos = PlayInfo.get_list()
                self.assertEqual(1, len(play_infos))
                self.assertNotEqual("", play_infos[0].data)
                song = Song.get_one(pk=2)
                self.assertGreater(song.listened_time, 0)
                return
        raise Exception("Did not play next...")

    def test_play_from_queue(self):
        self._setup_playlist()
        self.core.playback_manager._run_command('play')
        status = self.core.playback_manager._get_status()
        self.assertEqual('playing', status['status'])
        self.assertEqual(1, status['in_playlist_pk'])

        self.core.playback_manager._run_command('enqueue', 5)
        status = self.core.playback_manager._get_status()
        self.assertEqual('playing', status['status'])
        self.assertEqual([5], status['front_queue'])

        self.core.playback_manager._run_command('play_next')
        status = self.core.playback_manager._get_status()
        self.assertEqual('playing', status['status'])
        self.assertEqual(5, status['in_playlist_pk'])

        self.core.playback_manager._run_command('play_next')
        play_info = PlayInfo.get_last().was_queued
        self.assertEqual(True, play_info)

    def test_play_play_previous(self):
        self._setup_playlist()
        self.core.playback_manager._run_command('play_previous')
        status = self.core.playback_manager._get_status()
        self.assertEqual('playing', status['status'])
        self.assertEqual(5, status['in_playlist_pk'])
        self.core.playback_manager._run_command('play_previous')
        status = self.core.playback_manager._get_status()
        self.assertEqual('playing', status['status'])
        self.assertEqual(4, status['in_playlist_pk'])

    def tearDown(self):
        self.core.playback_manager.stop()
        super(TestPlaybackManager, self).tearDown()


class TestCuePlayback(BaseCleanOnEach):
    def setUp(self):
        super(TestCuePlayback, self).setUp()

        config_info = ConfigInfo.get_or_create(name="lib_handler.file_system")
        self.lib_path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), "test_library")
        config_info.update(data_blob=json.dumps({"monitored_dirs": self.lib_path}))
        self.target_path = os.path.join(self.lib_path, 'test2.mp3')
        copyfile(os.path.join(self.lib_path, 'test.mp3'), self.target_path)

        self.core = Core()
        self.core._library_handlers[0].rescan()
        self.core._library_handlers[0].stop_process()
        song = Song.get_one(title="TestTrack1")
        self.assertEqual(('TestArtist', "TestAlbum", "TestTrack1"), (song.artist, song.album, song.title))
        self.song1_pk = song.pk
        song = Song.get_one(title="TestTrack2")
        self.assertEqual(('TestArtist', "TestAlbum", "TestTrack2"), (song.artist, song.album, song.title))
        self.song2_pk = song.pk
        self.core.playback_manager.start()
        self.core.playback_manager._run_command("set_repeat_mode", "repeat-all")

    def tearDown(self):
        self.core.playback_manager.stop()

    def test_play_cue(self):
        self.assertEqual(SongLink.get_one(song_id=self.song1_pk).start, 0)
        self.core.playback_manager._run_command('play', self.song1_pk)
        status = self.core.playback_manager._get_status()
        self.assertEqual('playing', status['status'])
        self.assertEqual(self.song1_pk, status['in_playlist_pk'])
        self.assertTrue(0 <= status['position'] < 0.2, status['position'])

        flag = False
        for i in range(200):
            time.sleep(0.1)
            status = self.core.playback_manager._get_status()
            self.assertEqual('playing', status['status'])
            if self.song2_pk == status['in_playlist_pk']:
                flag = True
                l_time = Song.get_one(pk=self.song1_pk).listened_time
                self.assertGreater(l_time, 3.4)
            if flag and status['in_playlist_pk'] != self.song2_pk:
                l_time = Song.get_one(pk=self.song2_pk).listened_time
                self.assertTrue(0 <= status['position'] < 0.2)
                self.assertGreater(l_time, 1.8)
                return
        raise Exception("Test Failed...")

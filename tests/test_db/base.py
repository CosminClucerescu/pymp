import unittest

from pymp.db import ModelBase, Song
from pymp.db.models.base import SESSION


class _Base(unittest.TestCase):
    @classmethod
    def _clean_db(cls):
        SESSION.engine.dispose()
        ModelBase.metadata.drop_all(bind=SESSION.engine)
        SESSION.drop_all()
        SESSION.session.commit()
        ModelBase.metadata.create_all(bind=SESSION.engine)


class BaseCleanOnClass(_Base):
    @classmethod
    def setUpClass(cls):
        cls._clean_db()
        super(BaseCleanOnClass, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls._clean_db()


class BaseCleanOnEach(_Base):

    def setUp(self):
        self._clean_db()
        self._null_song = Song.get_or_create(artist='NONE', title='NONE', album='NONE',
                                             data_blob='{}', track_number=0, disabled=True)  # type: Song

    def tearDown(self):
        self._clean_db()

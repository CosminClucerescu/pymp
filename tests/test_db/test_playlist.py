"""
    Test the base model, but as there's no table for it use an alternate model.
"""
from pymp.db import PlayListEntry, Playlist, Song
from tests.test_db.base import BaseCleanOnEach


class TestGetList(BaseCleanOnEach):
    def setUp(self):
        self._clean_db()
        self.playlist = Playlist.create(name="Test")
        for index in range(100):
            song = Song.create('artist', str(index), '{}'.format(index % 2), '{}', 0)
            if index % 2 == 0:
                PlayListEntry.create(playlist_id=self.playlist.pk, song_id=song.pk)

    def test_get_song_list(self):
        # as this is done through get_paginated there's no point of retesting those features
        self.assertTrue(PlayListEntry.get_list())
        song_list, total = self.playlist.get_song_list(1, 100)
        self.assertEqual(50, total)
        self.assertEqual(50, len(song_list))
        for song, in_playlist_pk in song_list:
            self.assertEqual(0, int(song.title) % 2)

    def test_update(self):
        playlist = Playlist.create(name='Test2')
        songs = Song.get_list()
        self.assertEqual(100, len(songs))
        song_list, total = playlist.get_song_list(1, 500)
        self.assertEqual(0, total)
        self.assertEqual(0, len(song_list))

        playlist.update([song.pk for song in songs])
        song_list, total = playlist.get_song_list(1, 500)
        self.assertEqual(100, total)
        self.assertEqual(100, len(song_list))

    def test_multiple_playlists(self):
        playlist = Playlist.create(name='Test3')
        songs = Song.get_list()
        self.assertEqual(100, len(songs))
        playlist.update([song.pk for song in songs])

        song_list, total = playlist.get_song_list(1, 500)
        self.assertEqual(100, total)
        self.assertEqual(100, len(song_list))

        song_list, total = self.playlist.get_song_list(1, 100)
        self.assertEqual(50, total)
        self.assertEqual(50, len(song_list))

    def test_get_next_song(self):
        self.assertEqual(2, self.playlist.get_next_song(1)[1])
        self.assertEqual((None, None), self.playlist.get_next_song(100))

    def test_get_previous_song(self):
        self.assertEqual(1, self.playlist.get_previous_song(2)[1])
        self.assertEqual((None, None), self.playlist.get_previous_song(1))

    def test_get_song(self):
        song, playlist_pk = self.playlist.get_song(1)
        self.assertEqual(1, playlist_pk)
        self.assertEqual(1, song.pk)
        song, playlist_pk = self.playlist.get_song(2)
        self.assertEqual(2, playlist_pk)
        self.assertEqual(3, song.pk)
        self.assertEqual((None, None), self.playlist.get_song(100))

    def test_get_last(self):
        entry = self.playlist.get_last_song()
        self.assertEqual(99, entry[0].pk)
        self.assertEqual('98', entry[0].title)
        self.assertEqual(50, entry[1])

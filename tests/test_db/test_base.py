"""
    Test the base model, but as there's no table for it use an alternate model.
"""
from types import GeneratorType

from pymp.db.models.base import MAX_PAGINATION_COUNT
from pymp.db import Song
from tests.test_db.base import BaseCleanOnClass, BaseCleanOnEach


class TestGetList(BaseCleanOnClass):
    @classmethod
    def setUpClass(cls):
        super(TestGetList, cls).setUpClass()
        for index in range(10):
            Song.create('artist', str(index), '{}'.format(index % 2), '{}', 0)

    def test_get_all(self):
        self.assertEqual(10, len(Song.get_list()))

    def test_single_element(self):
        self.assertEqual(Song.get_one(pk=1), Song.get_list(pk=1)[0])

    def test_no_results(self):
        self.assertEqual([], Song.get_list(album='nonne'))

    def test_multiple_elements(self):
        self.assertEqual(5, len(Song.get_list(album='0')))

    def test_multi_query(self):
        self.assertEqual(1, len(Song.get_list(album='0', title='2')))


class TestGetOne(BaseCleanOnClass):
    def test_ignore_case(self):
        Song.create(artist='artist', title='title', album='abc', data_blob='{}', track_number=0)
        self.assertTrue(Song.get_one(raise_error=False, ignore_case=True, artist='artist', title='TITLE'))
        self.assertFalse(Song.get_one(raise_error=False, ignore_case=True, artist='ARTIST', title='TITLEu'))

        self.assertTrue(Song.get_one(ignore_case=True, title='TITLE'))
        self.assertTrue(Song.get_one(raise_error=False, ignore_case=True))

    def test_ignore_case_unicode(self):
        Song.create(artist='あるちとabc', title='タイトルABC', album='abc', data_blob='{}', track_number=0)
        self.assertTrue(Song.get_one(raise_error=False, ignore_case=True, artist='あるちとABC', title='タイトルabc'))
        self.assertFalse(Song.get_one(raise_error=False, ignore_case=True, artist='あるちと', title='タイトルABC'))

    def test_more_than_one(self):
        Song.create(artist='artist', title='title', album='abc', data_blob='{}', track_number=0)
        Song.create(artist='artist1', title='title', album='abc', data_blob='{}', track_number=0)
        with self.assertRaises(Song.FoundMoreThanOne):
            Song.get_one(title='title')


class TestGetPaginated(BaseCleanOnClass):

    @classmethod
    def setUpClass(cls):
        super(TestGetPaginated, cls).setUpClass()
        for index in range(MAX_PAGINATION_COUNT * 2 + 50):
            Song.create('artist', "t{}".format(index), 'album', '{}', 0)

    def test_max_count(self):
        page, total = Song.get_paginated(1, MAX_PAGINATION_COUNT * 2)
        self.assertEqual(len(Song.get_list()), total)
        self.assertEqual(MAX_PAGINATION_COUNT, len(page))

    def test_all_items(self):
        total_count = 0
        for index in range(1, 4):
            page, total = Song.get_paginated(index, MAX_PAGINATION_COUNT)
            total_count += len(page)
        self.assertEqual(len(Song.get_list()), total_count)

    def test_not_found(self):
        with self.assertRaises(Song.NotFound):
            Song.get_paginated(33, MAX_PAGINATION_COUNT)

    def test_last_page(self):
        self.assertEqual(50, len(Song.get_paginated(3, MAX_PAGINATION_COUNT)[0]))

    def test_filtering(self):
        print([song.title for song in Song.get_paginated(1, MAX_PAGINATION_COUNT, 't1000', disabled=False)[0]])
        self.assertEqual(1, len(Song.get_paginated(1, MAX_PAGINATION_COUNT, 't1000', disabled=False)[0]))
        self.assertEqual(61, len(Song.get_paginated(1, MAX_PAGINATION_COUNT, 't10', disabled=False)[0]))
        self.assertEqual(161, len(Song.get_paginated(1, MAX_PAGINATION_COUNT, 't1', disabled=False)[0]))


class TestGetNext(BaseCleanOnClass):
    @classmethod
    def setUpClass(cls):
        super(TestGetNext, cls).setUpClass()
        for index in range(50):
            Song.create('artist', "t{}".format(index), 'album', '{}', 0)

    def test_no_query(self):
        self.assertEqual(2, Song.get_next(1, "").pk)

    def test_filter(self):
        song = Song.get_next(1, "t1")
        self.assertEqual('t1', song.title)
        self.assertEqual('t10', Song.get_next(song.pk, "t1").title)

    def test_none(self):
        self.assertIsNone(Song.get_next(100, ''))


class TestGetPrevious(BaseCleanOnClass):
    @classmethod
    def setUpClass(cls):
        super(TestGetPrevious, cls).setUpClass()
        for index in range(50):
            Song.create('artist', "t{}".format(index), 'album', '{}', 0)

    def test_no_query(self):
        self.assertEqual(1, Song.get_previous(2, "").pk)
        last = Song.get_last()
        self.assertEqual(last.pk - 1, Song.get_previous(last.pk, "").pk)

    def test_filter(self):
        song = Song.get_next(Song.get_next(1, "t1").pk, "t1")
        self.assertEqual('t1', song.get_previous(song.pk, 't1').title)

    def test_none(self):
        self.assertIsNone(Song.get_previous(1, ''))


class TestOther(BaseCleanOnEach):
    # test other function that don't deserve their own TestCase
    def setUp(self):
        for index in range(50):
            Song.create('artist', "t{}".format(index), 'album', '{}', 0)

    def test_delete(self):
        Song.get_one(pk=1).delete()
        with self.assertRaises(Song.NotFound):
            Song.get_one(pk=1)

    def test_get_by_query(self):
        self.assertIsInstance(Song.get_by_query(None), GeneratorType)

    def test_get_last(self):
        self.assertEqual(50, Song.get_last().pk)

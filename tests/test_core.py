# coding=utf-8
import json
import os
from threading import Thread
from unittest import TestCase

import websocket

from pymp.__main__ import Core, NotAllowed, DuplicateCommand, AlreadyRunning, WebSocketListener
from pymp.commands import ParameterDescription
from pymp.db import Song
from tests.test_db.base import BaseCleanOnClass

AVAILABLE_COMMANDS = {'command': 'core.get_available_commands',
                      'result': [{'description': 'Trigger a rescan of the media library.',
                                  'name': 'file_system.rescan',
                                  'parameters': {}},
                                 {'description': 'Add a new directory to the list of monitored ones',
                                  'name': 'file_system.add_monitored_dir',
                                  'parameters': {'path': 'Path of the directory to be added.'}},
                                 {'description': 'Remove a directory from the list of monitored '
                                                 'ones',
                                  'name': 'file_system.remove_monitored_dir',
                                  'parameters': {'path': 'Path of the directory to be added.'}},
                                 {'description': 'Get the list of monitored dirs',
                                  'name': 'file_system.get_monitored_dirs',
                                  'parameters': {}},
                                 {'description': 'Trigger play/pause base on current state.',
                                  'name': 'playback.play_pause',
                                  'parameters': {}},
                                 {'description': 'Play the next song.',
                                  'name': 'playback.play_next',
                                  'parameters': {}},
                                 {'description': 'Play the previous song.',
                                  'name': 'playback.play_previous',
                                  'parameters': {}},
                                 {'description': 'Play the given song.',
                                  'name': 'playback.play',
                                  'parameters': {'pk': 'pk inside the current playlist.'}},
                                 {'description': 'Stop playback',
                                  'name': 'playback.stop',
                                  'parameters': {}},
                                 {'description': 'Switch playback to another playlist.',
                                  'name': 'playback.change_playlist',
                                  'parameters': {'pk': 'pk of the new  playlist.'}},
                                 {'description': 'Change the playback query.',
                                  'name': 'playback.change_query',
                                  'parameters': {'query': 'A query string to filter the playlist '
                                                          'used for playback.'}},
                                 {'description': 'Change the repeat mode of the playback.',
                                  'name': 'playback.change_repeat_mode',
                                  'parameters': {'new_mode': ['repeat-one',
                                                              'no-repeat',
                                                              'repeat-all']}},
                                 {'description': 'Get information about the status of the player',
                                  'name': 'playback.get_status',
                                  'parameters': {}},
                                 {'description': 'Enqueue a song to be played.',
                                  'name': 'playback.enqueue',
                                  'parameters': {'pk': 'pk inside the current playlist.'}},
                                 {'description': 'Get songs from the currently playing playlist.',
                                  'name': 'playback.get_song_list',
                                  'parameters': {'count': 'Number of results to return',
                                                 'page': 'Page to return',
                                                 'query': 'Use this query instead of the playlist '
                                                          'one.'}},
                                 {'description': 'Set the playback position',
                                  'name': 'playback.set_position',
                                  'parameters': {'position': 'New position as a float invalid '
                                                             'values shall be ignored. '}},
                                 {'description': 'Set the volume.',
                                  'name': 'playback.set_volume',
                                  'parameters': {'volume': 'New volume between 0 and 1, invalid '
                                                           'values shall be ignored.'}},
                                 {'description': 'Get a list of available commands',
                                  'name': 'core.get_available_commands',
                                  'parameters': {}},
                                 {'description': 'Update a song',
                                  'name': 'core.update_song',
                                  'parameters': {'pk': 'Pk of the song to update',
                                                 'rating': 'New rating for the song'}}],
                      'status': 'success'}


class TestCommandRegistration(TestCase):
    core = None

    @classmethod
    def setUpClass(cls):
        cls.core = Core()
        cls.core.start()

    @classmethod
    def tearDownClass(cls):
        cls.core.stop()

    def test_register_commands_not_allowed(self):
        # commands should be registered during a brief window only (on_load)
        with self.assertRaises(NotAllowed):
            self.core.register_command("", "", None)

    def test_duplicate_command(self):
        self.core._register_caller = 'test'
        try:
            self.core.register_command("", "", None)
            with self.assertRaises(DuplicateCommand):
                self.core.register_command("", "", None)
        finally:
            self.core._register_caller = None


class TestCommandExecution(BaseCleanOnClass):
    core = None

    @classmethod
    def setUpClass(cls):
        super(TestCommandExecution, cls).setUpClass()
        Song.get_or_create(artist='NONE', title='NONE', album='NONE',
                           data_blob='{}', track_number=0, disabled=True)  # type: Song
        cls.core = Core()
        cls.core.start()

    @classmethod
    def tearDownClass(cls):
        cls.core.stop()

    def test_all(self):
        data = dict(self.core.execute_command(json.dumps({"name": "core.get_available_commands"})))
        failed_commands = []
        for command in data['result']:
            params = dict(command['parameters'])
            for param, val in params.items():
                if isinstance(val, ParameterDescription):
                    params[param] = val.example
            response = self.core.execute_command(json.dumps({"name": command['name'],
                                                             'parameters': params}))
            if response['status'] != 'success':
                failed_commands.append((command['name'], response))
        self.assertFalse(failed_commands)

    def test_get_available_commands(self):
        data = self.core.execute_command(json.dumps({"name": "core.get_available_commands"}))
        self.assertEqual(AVAILABLE_COMMANDS, data)

    def test_bad_command_format(self):
        data = self.core.execute_command(json.dumps({"name": "core.get_available_commands"}) + 'x')
        self.assertEqual({'message': 'Failed to understand the given command string.',
                          'status': 'failed'}, data)

    def test_invalid_command(self):
        data = self.core.execute_command(json.dumps({"name": "core.get_available_commandss"}))
        self.assertEqual({'command': 'core.get_available_commandss',
                          'message': 'No such command.',
                          'status': 'failed'}, data)

    def test_fail_to_execute_command(self):
        data = self.core.execute_command(json.dumps({"name": "core.get_available_commands",
                                                     "parameters": {"abc": "abc", "bca": "bca"}}))
        self.assertEqual({'command': 'core.get_available_commands',
                          'error': "['Received Unknown parameter abc']",
                          'message': 'Failed to execute command',
                          'status': 'failed'}, data)


class TestPidHandling(TestCase):
    core = None

    @classmethod
    def setUpClass(cls):
        cls.core = Core()
        cls.core.start()

    @classmethod
    def tearDownClass(cls):
        cls.core.stop()

    def test_fails_to_start_twice(self):
        with self.assertRaises(AlreadyRunning):
            self.core.start()

    def test_cleans_up_pid(self):
        self.core.stop()
        with open('pymp_lock.pid', 'w') as file_:
            file_.write("0")
        self.core.start()

    def test_stops_without_pid_file(self):
        # Should not fail to stop if the pid files no longer exists
        os.remove('pymp_lock.pid')
        self.core.stop()
        self.core.start()


class TestSocketListener(TestCase):
    socket_listener = None
    _thread = None
    core = None

    def setUp(self):
        super(TestSocketListener, self).setUp()
        WebSocketListener.LISTEN_PORT += 1
        self.core = Core()
        self.socket_listener = WebSocketListener(self.core)
        self._thread = Thread(target=self.socket_listener.run)
        self._thread.start()
        self.client = websocket.WebSocket()
        self.core.start()
        try:
            self.client.connect("ws://127.0.0.1:{}".format(WebSocketListener.LISTEN_PORT))
        except Exception:
            self.socket_listener.stop()
            self.core.stop()
            self._thread.join()
            raise

    def send(self, data):
        self.client.send(json.dumps(data))
        return json.loads(self.client.recv())

    def tearDown(self):
        self.client.close()
        self.socket_listener.stop()
        self._thread.join()

    def test_all(self):
        data = dict(self.core.execute_command(json.dumps({"name": "core.get_available_commands"})))
        failed_commands = []
        for command in data['result']:
            params = dict(command['parameters'])
            for param, val in params.items():
                if isinstance(val, ParameterDescription):
                    params[param] = val.example
            response = self.send({"name": command['name'],
                                  'parameters': params})
            if response['status'] != 'success':
                failed_commands.append((command['name'], response))
        self.assertFalse(failed_commands)

    def test_get_available_commands(self):
        data = self.send({"name": "core.get_available_commands"})
        self.assertEqual(AVAILABLE_COMMANDS, data)

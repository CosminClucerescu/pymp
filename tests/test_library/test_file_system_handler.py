# coding=utf-8
import json
import os
import time
from datetime import datetime
from shutil import copyfile, rmtree

from pymp.db import SongLink, ConfigInfo, Song
from pymp.__main__ import Core
from tests.test_db.base import BaseCleanOnEach


class TestFileSystemHandler(BaseCleanOnEach):
    @classmethod
    def setUpClass(cls):
        cls.core = Core()

    def setUp(self):
        super(TestFileSystemHandler, self).setUp()
        try:
            os.mkdir("/tmp/pymp_test_dir")
        except OSError as err:
            if err.errno != 17:  # File Exists
                raise
        try:
            os.mkdir("/tmp/pymp_test_dir/fake")
        except OSError as err:
            if err.errno != 17:  # File Exists
                raise
        self.file_system_handler = self.core._library_handlers[0]
        self.file_system_handler.rescan()
        self.assertEqual([], self.file_system_handler._monitored_dirs, "Database not properly cleaned..")

    def tearDown(self):
        rmtree('/tmp/pymp_test_dir')
        self.file_system_handler.stop_process()
        super(TestFileSystemHandler, self).tearDown()

    def test_add_subdir_and_remove_root(self):
        # Check that nothing gets removed from the db for the subdir when removing it's root but keeping it.
        # i.e / -> /home then nothing should get remove from /home as it's still monitored but everything else
        #  from / should be removed
        config_info = ConfigInfo.get_or_create(name="lib_handler.file_system")
        config_info.update(data_blob=json.dumps({"monitored_dirs": "/tmp/pymp_test_dir"}))
        self.file_system_handler.rescan()
        self.assertEqual(["/tmp/pymp_test_dir"], self.file_system_handler._monitored_dirs)
        init_link = SongLink.create(self._null_song.pk, "/tmp/pymp_test_dir/fake", 'file_system', "", 0)
        SongLink.create(self._null_song.pk, "/tmp/fake", 'file_system', "", duration=0)
        self.file_system_handler.rescan()
        self.file_system_handler.stop_process()
        self.assertEqual(init_link.pk, SongLink.get_one(link="/tmp/pymp_test_dir/fake").pk)
        self.assertIsNone(SongLink.get_one(raise_error=False, link="/tmp/fake"))

    def _setup_mp3(self):
        config_info = ConfigInfo.get_or_create(name="lib_handler.file_system")
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test.mp3')
        copyfile(path, '/tmp/pymp_test_dir/test.mp3')
        config_info.update(data_blob=json.dumps({"monitored_dirs": '/tmp/pymp_test_dir'}))
        self.file_system_handler.rescan()
        self.file_system_handler.stop_process()

    def test_creates_song(self):
        self._setup_mp3()
        song = Song.get_one(pk=2)
        self.assertEqual(('TestArtist', "TestAlbum", "TestTitle"), (song.artist, song.album, song.title))
        song_link = SongLink.get_one(song_id=song.pk)
        self.assertEqual('/tmp/pymp_test_dir/test.mp3', song_link.link)
        self.assertEqual(5, song_link.duration)

    def test_time_stamping(self):
        self._setup_mp3()
        song = Song.get_one(pk=2)
        song_link = SongLink.get_one(song_id=song.pk)
        config_info = ConfigInfo.get_or_create(name="lib_handler.file_system")
        self.assertEqual({'last_run': self.file_system_handler.scanner_processes[0]._start,
                          'monitored_dirs': '/tmp/pymp_test_dir'},
                         json.loads(config_info.data_blob))
        self.assertEqual(int(self.file_system_handler.scanner_processes[0]._start),
                         int(os.stat(song_link.link).st_atime))

    def test_read_only_changed_files(self):
        config_info = ConfigInfo.get_or_create(name="lib_handler.file_system")
        path = os.path.dirname(os.path.realpath(__file__))
        config_info.update(
            data_blob=json.dumps({"monitored_dirs": path, 'last_run': time.mktime(datetime.now().timetuple())}))
        self.file_system_handler.rescan()
        self.file_system_handler.stop_process()
        with self.assertRaises(Song.NotFound):
            Song.get_one(pk=2)


class TestCueHandling(BaseCleanOnEach):
    def setUp(self):
        super(TestCueHandling, self).setUp()
        self.core = Core()
        self.file_system_handler = self.core._library_handlers[0]
        self.lib_path = os.path.dirname(os.path.realpath(__file__))
        self.target_path = os.path.join(self.lib_path, 'test2.mp3')
        if not os.path.exists(self.target_path):
            copyfile(os.path.join(self.lib_path, 'test.mp3'), self.target_path)

    def tearDown(self):
        os.remove(self.target_path)

    def test_creates_song(self):
        config_info = ConfigInfo.get_or_create(name="lib_handler.file_system")

        config_info.update(data_blob=json.dumps({"monitored_dirs": self.lib_path}))
        self.file_system_handler.rescan()
        self.file_system_handler.stop_process()
        cue_song1 = Song.get_one(title="TestTrack1")
        self.assertEqual(('TestArtist', "TestAlbum", "TestTrack1"),
                         (cue_song1.artist, cue_song1.album, cue_song1.title))
        song_link = SongLink.get_one(song_id=cue_song1.pk)
        self.assertEqual(self.target_path, song_link.link)
        self.assertEqual(0, song_link.start)
        self.assertEqual(3.5, song_link.end)
        self.assertEqual(3.5, song_link.duration)

        cue_song1 = Song.get_one(title="TestTrack2")
        self.assertEqual(('TestArtist', "TestAlbum", "TestTrack2"),
                         (cue_song1.artist, cue_song1.album, cue_song1.title))
        song_link = SongLink.get_one(song_id=cue_song1.pk)
        self.assertEqual(self.target_path, song_link.link)
        self.assertEqual(3.5, song_link.start)
        self.assertEqual(song_link.TRACK_END, song_link.end)
        self.assertEqual(1.5, song_link.duration)

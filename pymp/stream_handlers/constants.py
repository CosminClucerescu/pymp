class PlayStates:
    STOPPED = 'stopped'
    PAUSED = 'paused'
    PLAYING = 'playing'


class RepeatOptions(list):
    ONE = 'repeat-one'
    ALL = 'repeat-all'
    NO = 'no-repeat'

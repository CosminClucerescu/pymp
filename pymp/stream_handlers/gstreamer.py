# pylint: disable=C0413
"""Gstreamer API."""
import urllib
import urllib.request
import gi

gi.require_version('Gst', '1.0')  # noqa
from gi.repository import GLib, Gst
from .constants import PlayStates

_FORMAT_TIME = Gst.Format(Gst.Format.TIME)
_NANOSEC_MULT = 10 ** 9


class GstreamerStreamHandler:
    """
        StreamHandler wrapping over Gstreamer, Gstreamer should be run in another process but this is achieved by
        running the playbackManager in another process.
    """

    def __init__(self, stop_callback):
        """Initialize process."""
        Gst.init(None)
        self.state = PlayStates.STOPPED
        self._player = Gst.ElementFactory.make('playbin3', 'player')
        bus = self._player.get_bus()
        bus.add_signal_watch()
        bus.connect('message', self._on_message)
        fsink = Gst.ElementFactory.make('fakesink', 'fsink')
        self._player.set_property('video-sink', fsink)
        self.loop = GLib.MainLoop()
        self._stop_callback = stop_callback

    def loop_callback(self):
        context = self.loop.get_context()
        if context.pending():
            context.iteration()

    def play(self, uri=None):
        if uri:
            try:
                local_path, _ = urllib.request.urlretrieve(uri)
            except Exception:
                local_path = uri
            self._player.set_state(Gst.State.NULL)
            self._player.set_property('uri', 'file://{}'.format(local_path))
        self.state = PlayStates.PLAYING
        self._player.set_state(Gst.State.PLAYING)

    def pause(self):
        if self.state == PlayStates.PLAYING:
            self._player.set_state(Gst.State.PAUSED)
            self.state = PlayStates.PAUSED

    def stop(self):
        urllib.request.urlcleanup()
        self._player.set_state(Gst.State.NULL)
        self.state = PlayStates.STOPPED

    def set_position(self, position):
        if position > self.duration:
            return
        position_ns = position * _NANOSEC_MULT
        self._player.seek_simple(_FORMAT_TIME, Gst.SeekFlags.FLUSH, position_ns)

    def set_volume(self, volume):
        self._player.set_property('volume', volume)

    @property
    def duration(self):
        self._player.do_get_state(self._player, Gst.CLOCK_TIME_NONE)
        duration = 0
        if self.state != PlayStates.STOPPED:
            resp = self._player.query_duration(_FORMAT_TIME)
            duration = resp[1] // _NANOSEC_MULT
        return duration

    @property
    def position(self):
        self._player.do_get_state(self._player, Gst.CLOCK_TIME_NONE)
        position = 0
        if self.state != PlayStates.STOPPED:
            resp = self._player.query_position(_FORMAT_TIME)
            position = resp[1] // _NANOSEC_MULT
        return position if position > 0 else 0

    def _on_message(self, _, message):
        """When a message is received from Gstreamer."""
        if message.type in [Gst.MessageType.EOS,
                            Gst.MessageType.ERROR] or self.position >= self.duration > 0:
            pos = self.position
            duration = self.duration
            self.stop()
            self._stop_callback((message.type, pos, duration))

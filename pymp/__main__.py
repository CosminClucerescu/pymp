# coding=utf-8
import json
import os
import traceback

import psutil
from websocket_server import WebsocketServer

from pymp.commands import Command, ParameterDescription
from pymp.db import Song
from pymp.playback_manager import PlaybackManager
from pymp.library.handler.file_system import FileSystemHandler


class NotAllowed(Exception):
    pass


class DuplicateCommand(Exception):
    pass


class AlreadyRunning(Exception):
    pass


class Core:
    def __init__(self):
        self._commands = {}
        self._started = False
        self._register_caller = FileSystemHandler.name
        self._library_handlers = [FileSystemHandler(self)]  # TODO: Load from config or db
        self._register_caller = 'playback'
        self.playback_manager = PlaybackManager(self)  # TODO: Load from config
        self._register_caller = None
        self._register_commands()

    def _handle_pid(self):
        if not os.path.exists('pymp_lock.pid'):
            self._create_pid_file()
        else:
            with open('pymp_lock.pid') as lock_file:
                try:
                    pid = int(lock_file.read().strip())
                    if not self._check_pid(pid):
                        self._create_pid_file()
                    else:
                        raise AlreadyRunning("Already running with pid {}".format(pid))
                except ValueError:
                    self._create_pid_file()
        return True

    @staticmethod
    def _check_pid(pid):
        try:
            process = psutil.Process(pid)
        except psutil.NoSuchProcess:
            return False
        process_name = process.name()
        if process_name == psutil.Process().name():
            return True
        return False

    def _create_pid_file(self):
        pid = str(os.getpid())
        with open('pymp_lock.pid', 'w') as pid_file:
            pid_file.write(pid)
        self._started = True

    def start(self):
        self._handle_pid()
        for handler in self._library_handlers:
            handler.start_process()
        self.playback_manager.start()

    def stop(self):
        if self._started:
            try:
                os.remove('pymp_lock.pid')
            except OSError:
                pass
            self.playback_manager.stop()
            for library_handler in self._library_handlers:
                library_handler.stop_process()

    def execute_command(self, string):
        bad_command = {"message": "Failed to understand the given command string.",
                       "status": 'failed'}
        try:
            command_dict = json.loads(string)
        except ValueError:
            return bad_command
        if isinstance(command_dict, str):
            return bad_command
        command_name = command_dict.get('name', '')
        command = self._commands.get(command_name)
        if command is None:
            return {"message": "No such command.",
                    "status": 'failed',
                    "command": command_name}
        try:
            print("Executing command {}".format(command))
            r_val = {"command": command_name,
                     "status": "success",
                     "result": command.execute(command_dict.get('parameters', {}))}
            print("Success")
            return r_val
        except Exception as e:
            print("Failed")
            print(traceback.format_exc())
            return {"message": "Failed to execute command",
                    "error": str(e),
                    "status": "failed",
                    'command': command_name}

    def _get_command_list(self):
        return [command.as_dict() for command in self._commands.values()]

    @staticmethod
    def _valid_rating(rating):
        assert 0 <= int(rating) <= 10

    def _update_song(self, pk, rating):
        song = Song.get_one(pk=pk)
        song.update(rating=rating)
        if self.playback_manager.song and self.playback_manager.song['pk'] == pk:
            self.playback_manager.song['rating'] = rating

    def _register_commands(self):
        self._register_caller = 'core'
        self.register_command('get_available_commands', 'Get a list of available commands', self._get_command_list)
        self.register_command('update_song', 'Update a song', self._update_song,
                              {'pk': ParameterDescription('Pk of the song to update', 'pk', int, 1),
                               'rating': ParameterDescription('New rating for the song', 'rating',
                                                              self._valid_rating, 10)})
        self._register_caller = None

    def register_command(self, name, description, callback, expected_args=None):
        if self._register_caller is None:
            raise NotAllowed("Unexpected call to register_command after __init__ step.")
        key = "{}.{}".format(self._register_caller, name)
        if key in self._commands:
            raise DuplicateCommand("Command {} registered more than once".format(key))
        self._commands[key] = Command(key, callback, description, expected_args)


class WebSocketListener:
    LISTEN_PORT = 50000
    LISTEN_HOST = '0.0.0.0'

    def __init__(self, core):
        self.core = core
        super(WebSocketListener, self).__init__()
        self._clients = {}
        self._server = WebsocketServer(self.LISTEN_PORT, self.LISTEN_HOST)
        self._server.set_fn_new_client(self._handle_new_client)
        self._server.set_fn_client_left(self._handle_client_left)
        self._server.set_fn_message_received(self._handle_message)

    def _handle_new_client(self, client, _):
        self._clients[client['id']] = client

    def _handle_message(self, client, _, message):
        reply = self.core.execute_command(message)
        try:
            reply = json.dumps(reply)
        except Exception:
            reply = json.dumps({"message": "Failed to serialize response (server error).",
                                "status": 'failed',
                                "command": json.loads(message).get('name', '')})
        client['handler'].send_message(reply)

    def _handle_client_left(self, client, _):
        self._clients.pop(client['id'], [])

    def run(self):
        self._server.block_on_close = False
        self._server.run_forever()

    def stop(self):
        self._server.shutdown()
        self.core.stop()


def main():
    core = Core()
    core.start()
    WebSocketListener(core).run()


if __name__ == "__main__":
    main()

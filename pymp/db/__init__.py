# flake8: noqa
from .models.base import ModelBase, SESSION
from .models.config.config_info import ConfigInfo
from .models.library.song import Song
from .models.library.song_link import SongLink
from .models.stats.play_info import PlayInfo
from .models.stats.rating import RatingInfo
from .models.playlist import Playlist, PlayListEntry

ModelBase.metadata.create_all(bind=SESSION.engine)

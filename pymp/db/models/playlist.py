from sqlalchemy import Integer, Column, String, ForeignKey, and_

from .base import DB_SESSION, ModelBase
from .library.song import Song

__all__ = ['Playlist', 'PlayListEntry']


class PlayListEntry(ModelBase):
    __tablename__ = 'playlist_entry'
    pk = Column(Integer, primary_key=True)
    playlist_id = Column(Integer, ForeignKey('playlist.pk'))
    song_id = Column(Integer, ForeignKey('song.pk'))


def _get_playlist_song_handler(pl_pk):
    class PlaylistSong(PlayListEntry):
        # Can't think of a better name, model used to query Songs of a Playlist through what's implemented in BaseModel
        __abstract__ = True

        @classmethod
        def _query(cls):
            return DB_SESSION.query(Song, PlayListEntry).filter(
                and_(PlayListEntry.playlist_id == pl_pk, Song.pk == PlayListEntry.song_id))

        def __getitem__(self, item):
            return item

    return PlaylistSong


class Playlist(ModelBase):
    __tablename__ = 'playlist'
    pk = Column(Integer, primary_key=True)
    name = Column(String(256))
    _playlist_songs = None

    def init_on_load(self):
        self._playlist_songs = _get_playlist_song_handler(self.pk)

    class SongNotFound(Exception):
        pass

    def update(self, songs=None, __do_commit__=True, **kwargs):
        super(Playlist, self).update(__do_commit__=False)
        if songs is None:
            songs = []
        for song_id in songs:
            entry = PlayListEntry(playlist_id=self.pk, song_id=song_id)
            DB_SESSION.add(entry)
        if __do_commit__:
            DB_SESSION.commit()

    def get_song_list(self, page, count, query_string=None):
        songs, total = self._playlist_songs.get_paginated(page, count, query_string)
        return [(song[0], song[1].pk) for song in songs], total

    def get_next_song(self, pk, query_string=""):
        song = self._playlist_songs.get_next(pk, query_string)
        if not song:
            return None, None
        return song[0], song[1].pk

    def get_previous_song(self, pk, query_string=""):
        song = self._playlist_songs.get_previous(pk, query_string)
        if not song:
            return None, None
        return song[0], song[1].pk

    def get_song(self, pk):
        song = self._playlist_songs.get_one(raise_error=False, pk=pk)
        if not song:
            return None, None
        return song[0], song[1].pk

    def get_last_song(self):
        song = self._playlist_songs.get_last()
        if not song:
            return None, None
        return song[0], song[1].pk

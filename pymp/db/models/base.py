import os
from typing import Type, TypeVar, List, Generator, Tuple

from flask import Flask

from flask_sqlalchemy import SQLAlchemy

import sqlalchemy
from sqlalchemy import func, and_, or_, orm, exists
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import class_mapper

from werkzeug.exceptions import NotFound

T = TypeVar('T', bound='TrivialClass')


class _Base:
    __table_args__ = {'mysql_engine': 'InnoDB'}


_Base = declarative_base(cls=_Base)

_APP = Flask(__name__)
_APP.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQLALCHEMY_DATABASE_URI', "sqlite:///whatever.db")
_APP.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


class SQLAlchemy2(SQLAlchemy):
    def apply_driver_hacks(self, app, info, options):
        options.update({
            'isolation_level': 'READ COMMITTED' if 'sqlite' not in _APP.config[
                'SQLALCHEMY_DATABASE_URI'] else 'READ UNCOMMITTED',
        })
        super(SQLAlchemy2, self).apply_driver_hacks(app, info, options)


SESSION = SQLAlchemy2(_APP)
DB_SESSION = SESSION.session
MAX_PAGINATION_COUNT = 500


@_APP.teardown_request
def session_clear(exception=None):
    SESSION.session.remove()
    if exception and SESSION.session.is_active:
        SESSION.session.rollback()


class ModelBase(_Base):
    __abstract__ = True
    __order_on__ = None
    __group_by__ = None

    class NotFound(Exception):
        pass

    class FoundMoreThanOne(Exception):
        pass

    def init_on_load(self):
        pass

    @orm.reconstructor
    def _init_on_load(self):
        self.init_on_load()

    def to_dict(self):
        return {key: getattr(self, key) for key in self._model_attributes()}

    @classmethod
    def _query(cls):
        return DB_SESSION.query(cls)

    @classmethod
    def create(cls: Type[T], __do_commit__=True, **kwargs) -> T:
        instance = cls()
        DB_SESSION.add(instance)
        instance.update(__do_commit__=__do_commit__, **kwargs)
        instance.init_on_load()
        return instance

    @classmethod
    def get_or_create(cls: Type[T], **kwargs) -> T:
        try:
            return cls.get_one(**kwargs)
        except cls.NotFound:
            return cls.create(**kwargs)

    @classmethod
    def _get_query(cls, ignore_case, **kwargs):
        query_list = [
            func.lower(getattr(cls, key)) == value.lower() if ignore_case and isinstance(value, str)
            else getattr(cls, key) == value for key, value in kwargs.items()]
        if len(query_list) > 1:
            query_list = and_(*query_list)
        elif query_list:
            query_list = query_list[0]
        else:
            query_list = None
        return query_list

    @classmethod
    def get_list(cls: Type[T], ignore_case=False, **kwargs) -> List[T]:
        try:
            return list(cls.get_by_query(cls._get_query(ignore_case, **kwargs)))
        except Exception:
            SESSION.session.rollback()
            raise

    @classmethod
    def get_one(cls: Type[T], raise_error=True, ignore_case=False, **kwargs) -> T:
        all_ = cls.get_list(ignore_case, **kwargs)
        if not all_:
            if raise_error:
                raise cls.NotFound
            return None
        if raise_error and len(all_) > 1:
            raise cls.FoundMoreThanOne
        return all_[0]

    def update(self, __do_commit__=True, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
        if __do_commit__:
            DB_SESSION.commit()
        return self

    def delete(self):
        DB_SESSION.delete(self)
        DB_SESSION.commit()

    @classmethod
    def get_by_query(cls: Type[T], query) -> Generator[T, None, None]:
        if query is not None:
            data = cls._query().filter(query)
        else:
            data = cls._query()
        for db_object in data:
            yield db_object

    @classmethod
    def exists(cls: Type[T], query) -> bool:
        return DB_SESSION.query(exists().where(query)).scalar()

    @classmethod
    def delete_by_query(cls, query):
        cls._query().filter(query).delete(synchronize_session='fetch')
        DB_SESSION.commit()

    @classmethod
    def _model_attributes(cls):
        return [prop.key for prop in class_mapper(cls).iterate_properties
                if isinstance(prop, sqlalchemy.orm.ColumnProperty)]

    @classmethod
    def _get_attr_filter(cls, attr, string):
        if string.strip('"') == string:
            return getattr(cls, attr).contains(string)
        return getattr(cls, attr).__eq__(string.strip('"'))

    @classmethod
    def _get_search_filter(cls, string):
        """
        Search for string in any of the models attributes and return a list of matching instances.
        :param string:
        :return:
        """
        filter_list = []
        string = string.split(" AND ")
        query = {}
        all_ = []
        for element in string:
            try:
                key, value = element.split(':')
                try:
                    query[key].append(value)
                except KeyError:
                    query[key] = [value]
            except ValueError:
                all_.append(element)
        r_query = []
        if all_:
            for element in all_:
                for attr in cls._model_attributes():
                    filter_list.append(cls._get_attr_filter(attr, element))
            r_query.append(or_(*filter_list))
        if query:
            for key, value in query.items():
                if key not in cls._model_attributes():
                    continue
                if len(value) > 1:
                    value = or_(*[cls._get_attr_filter(key, val) for val in value])
                else:
                    value = cls._get_attr_filter(key, value[0])
                r_query.append(value)
        if len(r_query) > 1:
            return and_(*r_query)
        return r_query[0] if r_query else None

    @classmethod
    def get_paginated(cls: Type[T], page: int,
                      count: int, query_string: str = "", **query_kwargs) -> Tuple[List[T], int]:
        DB_SESSION.expire_all()
        count = min(MAX_PAGINATION_COUNT, count)
        query = cls._query().filter_by(**query_kwargs)
        if query_string:
            query = query.filter(cls._get_search_filter(query_string))
        if cls.__order_on__:
            query.order_by(cls.__order_on__)
        if cls.__group_by__:
            query.group_by(cls.__group_by__)
        try:
            pagination = query.paginate(page, count)
        except NotFound:
            raise cls.NotFound
        except Exception:
            SESSION.session.rollback()
            raise
        total = pagination.total

        return list(pagination.items), total

    @classmethod
    def get_next(cls: Type[T], pk: int, query_string: str, **query_kwargs) -> T:
        query = cls._query().filter_by(**query_kwargs).filter(and_(cls.pk > pk))
        if query_string:
            query = query.filter(cls._get_search_filter(query_string))
        entry = query.first()
        return entry

    @classmethod
    def get_previous(cls: Type[T], pk: int, query_string: str, **query_kwargs) -> T:
        query = cls._query().filter_by(**query_kwargs).filter(and_(cls.pk < pk))
        if query_string:
            query = query.filter(cls._get_search_filter(query_string))
        entry = query.order_by(cls.pk.desc()).first()
        return entry

    @classmethod
    def get_last(cls: Type[T]) -> T:
        return cls._query().order_by(cls.pk.desc()).first()

from sqlalchemy import Column, Integer, String, Text, Boolean
from sqlalchemy.orm import relationship

from ..base import ModelBase


class Song(ModelBase):
    __tablename__ = 'song'
    __order_on__ = 'track_number'
    __group_by__ = 'album'

    pk = Column(Integer, primary_key=True)
    artist = Column(String(1024))
    title = Column(String(1024))
    album = Column(String(1024))
    track_number = Column(Integer, nullable=True)
    data_blob = Column(Text)  # json blob with random data?
    rating = Column(Integer, default=0)
    disabled = Column(Boolean, default=False)
    links = relationship("SongLink", backref="song")
    listened_time = Column(Integer, default=0)

    @classmethod
    def get_or_create(cls, artist, title, album, data_blob, track_number, disabled=False):
        song_wrapper = Song.get_one(ignore_case=True, raise_error=False, artist=artist, title=title)
        if not song_wrapper:
            song_wrapper = Song.create(artist, title, album, data_blob, track_number, disabled)
        return song_wrapper

    @classmethod
    def create(cls, artist, title, album, data_blob, track_number, disabled=False):
        return super(Song, cls).create(artist=artist, title=title, album=album, data_blob=data_blob,
                                       track_number=track_number, disabled=disabled)

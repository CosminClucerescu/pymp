from sqlalchemy import Column, ForeignKey, String, Integer, Float, Text

from ..base import ModelBase


class SongLink(ModelBase):
    TRACK_END = -1
    __tablename__ = 'song_link'
    pk = Column(Integer, primary_key=True)
    song_id = Column(Integer, ForeignKey('song.pk'))
    link = Column(Text)
    handler = Column(String(64), nullable=True)
    duration = Column(Float)  # library duration in seconds
    # Also errors when handler is no longer registered?
    # Handler preferences x> y > etc
    handler_info = Column(Text)  # Some info to help identify links to delete when a handler changes its settings
    start = Column(Float, default=0)
    end = Column(Float, default=TRACK_END)

    # i.e. directory is removed from monitored dirs
    @classmethod
    def create(cls, song_id, link, handler, handler_info, duration, start=0, end=TRACK_END, __do_commit__=True):
        return super(SongLink, cls).create(song_id=song_id, link=link, handler=handler, handler_info=handler_info,
                                           duration=duration, start=start, end=end, __do_commit__=__do_commit__)

from sqlalchemy import Column, DateTime, Text, Integer, ForeignKey

from ..base import ModelBase


class RatingInfo(ModelBase):
    __tablename__ = 'rating'
    pk = Column(Integer, primary_key=True)
    song_id = Column(Integer, ForeignKey('song.pk'))
    date = Column(DateTime)
    score = Column(Integer)
    data = Column(Text)  # potential json data with details like: what parts where played etc etc.

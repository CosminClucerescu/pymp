from sqlalchemy import Integer, Column, DateTime, Text, func, Boolean, ForeignKey

from ..base import ModelBase


class PlayInfo(ModelBase):
    __tablename__ = 'play_info'
    pk = Column(Integer, primary_key=True)
    song_id = Column(Integer, ForeignKey('song.pk'))
    date = Column(DateTime, default=func.now())
    data = Column(Text)  # potential json data with details like: what parts where played etc etc.
    was_queued = Column(Boolean)

    @classmethod
    def create(cls, song_id, data, was_queued):
        return super(PlayInfo, cls).create(song_id=song_id, data=data, was_queued=was_queued)

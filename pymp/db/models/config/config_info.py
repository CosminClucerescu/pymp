from sqlalchemy import Column, Integer, String, Text

from ..base import ModelBase


class ConfigInfo(ModelBase):
    __tablename__ = 'config_info'
    pk = Column(Integer, primary_key=True)
    name = Column(String(64), unique=True)
    data_blob = Column(Text, default='{}')  # hold whatever for config info

    class DoesNotExist(Exception):
        pass

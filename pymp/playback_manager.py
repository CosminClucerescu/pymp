# coding=utf-8
import json
import traceback
from multiprocessing import JoinableQueue
from queue import Empty
from threading import Thread

from pymp.commands import ParameterDescription
from pymp.db import PlayInfo, SESSION
from .db import Song, Playlist
from .stream_handlers.constants import PlayStates, RepeatOptions
from .stream_handlers.gstreamer import GstreamerStreamHandler


class NotPlayable(Exception):
    pass


class PlayBackCommandError(Exception):
    def __init__(self, message, additional_data=None):
        super(PlayBackCommandError, self).__init__(message)
        self.additional_data = additional_data


def _get_play_list(pk=None):
    if not pk:
        return _LibraryAsPlaylist()
    return Playlist.get_one(raise_error=False, pk=pk) or _LibraryAsPlaylist()


class _LibraryAsPlaylist:
    SongNotFound = Song.NotFound
    pk = 0

    @staticmethod
    def get_song_list(*args, **kwargs):
        return [(song, song.pk) for song in Song.get_paginated(*args, **kwargs)[0]]

    @staticmethod
    def get_next_song(pk, query_string):
        song = Song.get_next(pk, query_string)
        if song:
            return song, song.pk
        return None, None

    @staticmethod
    def get_previous_song(pk, query_string):
        song = Song.get_previous(pk, query_string)
        if song:
            return song, song.pk
        return None, None

    @staticmethod
    def get_last_song():
        song = Song.get_last()
        if song:
            return song, song.pk
        return None, None

    @staticmethod
    def get_song(pk):
        song = Song.get_one(pk=pk)
        return song, song.pk

    @staticmethod
    def get_paginated(page, count, query_string=''):
        song_list, total = Song.get_paginated(page, count, query_string)
        r_song_list = [(song, song.pk) for song in song_list]
        return r_song_list, total


class CurrentlyPlayingInfo:
    # information about the currently playing song
    def __init__(self, playlist_entry, link, from_queue=False):
        self._song = playlist_entry[0]
        self.in_playlist_pk = playlist_entry[1]
        self._played_parts = [0, ]  # list of time intervals that were listened, should always come in pairs of 2
        # i.e 0, 150, 100, 150 Would mean that there was a set position at 150 to 100.
        self._last_update_idx = 0
        self._position = 0
        self.link = link
        if link:
            self.duration = link.duration
            self.start = link.start
        else:
            self.duration = 0
            self.start = 0
        self._was_queued = from_queue
        self._play_info = None
        self.song_pk = 0 if not self._song else self._song.pk
        self.entry = playlist_entry
        self.as_dict = self._song.to_dict() if self._song else None

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, val):
        self._position = abs(val - self.start)  # TODO: ....???

    def update_play_info(self, song_changed=False):
        if not self._song:
            return
        if not self._play_info:
            self._play_info = PlayInfo.create(self._song.pk, '{}', was_queued=self._was_queued)
        if song_changed:
            self._played_parts.append(self.position)
        time_passed = self._get_updated_listened_time()
        if time_passed:
            song = Song.get_one(pk=self.song_pk)
            song.update(listened_time=song.listened_time + time_passed)
            self._play_info.update(data=json.dumps({"played_parts": self._played_parts}))

    def _get_updated_listened_time(self):
        total = 0
        last_idx = len(self._played_parts)
        print(self._played_parts)
        for idx in range(self._last_update_idx, last_idx):
            try:
                total += self._played_parts[idx + 1] - self._played_parts[idx]
            except IndexError:
                break
        self._last_update_idx = last_idx
        return total

    def __bool__(self):
        return self.song_pk != 0

    def add_position_history(self, time, new_time=None):
        if new_time is None:
            new_time = time
        self._played_parts.append(time)
        self._played_parts.append(new_time)


class PlayBackThread(Thread):
    def __init__(self, command_queue):
        super(PlayBackThread, self).__init__()
        self._stream_handler = None  # type: GstreamerStreamHandler
        self._command_message_queue = command_queue  # type: JoinableQueue
        self.last_error = None

        self._playlist = _get_play_list()
        self.play_list_query = None

        self.repeat_mode = RepeatOptions.NO
        self.status = PlayStates.STOPPED
        self._do_stop = False

        self.currently_playing = CurrentlyPlayingInfo((None, 0), None)
        self.front_queue = []
        self.playlist_pk = self._playlist.pk
        self.volume = 1

    def _command_set_playlist_query(self, query):
        self.play_list_query = query

    def _command_change_playlist(self, playlist_pk=None):
        if not playlist_pk:
            self._playlist = _get_play_list()
            self.playlist_pk = self._playlist.pk
        else:
            # TODO: Too many silent failures...
            new_playlist = _get_play_list(playlist_pk)
            if not isinstance(new_playlist, _LibraryAsPlaylist):
                self.currently_playing = CurrentlyPlayingInfo((None, 0), None)  # TODO: To be stored on each playlist
                self.front_queue = []  # TODO: same as above
                self._playlist = new_playlist
                self.playlist_pk = self._playlist.pk
        print("Set playlist to {}".format(self._playlist.pk))

    def update_and_get_position(self):
        if self.status == PlayStates.STOPPED:
            self.currently_playing.position = 0
            return 0
        self.currently_playing.position = self._stream_handler.position
        return self.currently_playing.position

    def _stop_callback(self, err=None):
        if self._do_stop:
            return
        print("Stop called because of error {}".format(err))
        self.currently_playing.position = self.currently_playing.duration
        if self.status == PlayStates.PLAYING:
            try:
                self._command_play_next()
            except PlayBackCommandError:
                pass

    def _save_status(self):
        self.currently_playing.add_position_history(self.update_and_get_position())
        self.currently_playing.update_play_info()

    def stop(self):
        self._do_stop = True
        self.join()

    def run(self):
        SESSION.engine.dispose()
        self._stream_handler = GstreamerStreamHandler(self._stop_callback)
        if self.status == PlayStates.PLAYING:
            self._command_play(self.currently_playing.in_playlist_pk)

        while True:
            try:
                if self._do_stop:
                    self._save_status()
                    break
                if self.status == PlayStates.PLAYING and self.currently_playing\
                        and self.currently_playing.link.end != self.currently_playing.link.TRACK_END \
                        and self.currently_playing.position >= self.currently_playing.duration:
                    self._command_play_next()
                self._stream_handler.loop_callback()
                self.update_and_get_position()
                command, args = self._command_message_queue.get(True, 0.1)
                if not isinstance(args, (tuple, list)):
                    args = [args]
                print("Running command {}".format(command))
                getattr(self, "_command_{}".format(command))(*args)
                print("Finished command {}".format(command))
                self._command_message_queue.task_done()
            except Empty:
                pass
            except Exception as e:
                print(traceback.format_exc())
                self.last_error = e
                self._command_message_queue.task_done()

    def _command_stop(self):
        self._save_status()
        self.status = PlayStates.STOPPED
        self._stream_handler.stop()

    def _command_enqueue(self, pk):
        self.front_queue.append(pk)

    def _command_play_pause(self):
        if self.status == PlayStates.PLAYING:
            self.status = PlayStates.PAUSED
            self._command_pause()
            self._save_status()
        elif self.status == PlayStates.PAUSED:
            self._command_resume()
            self.status = PlayStates.PLAYING
        elif self.status == PlayStates.STOPPED:
            self._command_play(self.currently_playing.in_playlist_pk)

    def _command_resume(self):
        self._stream_handler.play()

    def _command_pause(self):
        self._stream_handler.pause()

    def _get_next_to_play(self, pk):
        song = self._playlist.get_next_song(pk, self.play_list_query)
        if not song[0]:
            if self.repeat_mode == RepeatOptions.ALL:
                song = self._playlist.get_next_song(0, self.play_list_query)
        return song

    def _get_previous_to_play(self, pk):
        song = self._playlist.get_previous_song(pk, self.play_list_query)
        if not song[0]:
            if self.repeat_mode == RepeatOptions.ALL:
                song = self._playlist.get_last_song()
        return song

    def _command_play(self, pk=None):
        if pk:
            try:
                song = self._playlist.get_song(pk=pk)
                self._play(song)
                return
            except (self._playlist.SongNotFound, NotPlayable):
                pass
        self._command_play_next()

    def _command_set_volume(self, volume):
        volume = min(volume, 1)
        volume = max(volume, 0)
        self.volume = volume
        self._stream_handler.set_volume(volume)
        return {'volume': volume}

    def _command_set_position(self, position):
        self._save_status()
        if position < 0:
            return
        self._stream_handler.set_position(position + self.currently_playing.start)
        self._save_status()

    def _play(self, playlist_entry, from_queue=False):
        song, in_playlist_pk = playlist_entry
        print("Trying to play {} {}".format(song.pk, in_playlist_pk))
        for link in song.links:
            if not link.duration:
                continue
            if self.status == PlayStates.PLAYING \
                    and self.currently_playing.link \
                    and link.link == self.currently_playing.link.link \
                    and abs(self._stream_handler.position - link.start) < 2:
                self.currently_playing.update_play_info(song_changed=True)
                self.currently_playing = CurrentlyPlayingInfo(playlist_entry, link, from_queue)
                return

            self.currently_playing.update_play_info(song_changed=True)
            self.currently_playing = CurrentlyPlayingInfo(playlist_entry, link, from_queue)

            self._stream_handler.play(link.link)
            self.status = PlayStates.PLAYING
            if link.start > 0:
                self._stream_handler.set_position(link.start)
            return
        raise NotPlayable("Song with the given pk {} is not playable".format(song.pk))

    def _command_play_next(self):
        while self.front_queue:
            try:
                self._play(self._playlist.get_song(pk=self.front_queue.pop()), from_queue=True)
                break
            except (self._playlist.SongNotFound, NotPlayable):
                print("Failed to play from queue.")
        else:
            if self.currently_playing and self.repeat_mode == RepeatOptions.ONE:
                try:
                    self._play(self.currently_playing.entry)
                except NotPlayable:
                    self._command_stop()
            else:
                self._try_to_play_until_playable_is_found(self._get_next_to_play)

    def _try_to_play_until_playable_is_found(self, _get_func):
        song = _get_func(self.currently_playing.in_playlist_pk)
        started_at = song
        if not song[0]:
            self._command_stop()
            return
        while True:
            try:
                self._play(song)
                return
            except NotPlayable:
                print("Failed to play {}".format(song[0].pk))
                song = _get_func(song[1])
                if not song[0] or song[1] == started_at[1]:
                    self._command_stop()
                    return

    def _command_play_previous(self):
        self._try_to_play_until_playable_is_found(self._get_previous_to_play)

    def _command_noop(self, ):
        #  Used to unblock the queue to update currently playing position
        pass

    def _command_set_repeat_mode(self, new_mode):
        if new_mode in [RepeatOptions.ONE, RepeatOptions.NO, RepeatOptions.ALL]:
            self.repeat_mode = new_mode


class PlaybackManager:
    def __init__(self, core):
        """
        :type core: pymp.core.__main__._Core
        """
        self._register_commands(core)
        self._command_message_queue = JoinableQueue()  # Send commands to the playback worker
        self._playback_thread = None  # type: PlayBackThread

    def _run_command(self, name, *args):
        if not self._playback_thread:
            raise Exception("Playback thread not started can't run command.")
        if not args:
            args = []
        self._command_message_queue.put((name, args))
        self._command_message_queue.join()  # Wait until the given task is done
        error = None
        if self._playback_thread.last_error:
            error = self._playback_thread.last_error
            self._playback_thread.last_error = None
        if error:
            raise Exception(str(error))

    @property
    def song(self):
        return self._playback_thread.currently_playing.as_dict

    def _get_status(self):
        self._run_command('noop')
        return {
            "in_playlist_pk": self._playback_thread.currently_playing.in_playlist_pk,
            "song_pk": self._playback_thread.currently_playing.song_pk,
            "position": self._playback_thread.update_and_get_position(),
            "duration": self._playback_thread.currently_playing.duration,
            "status": self._playback_thread.status,
            "front_queue": self._playback_thread.front_queue,
            "playlist_pk": self._playback_thread.playlist_pk,
            "playlist_query": self._playback_thread.play_list_query,
            "song": self.song,
            "repeat_mode": self._playback_thread.repeat_mode,
            "volume": self._playback_thread.volume
        }

    def start(self):
        self._playback_thread = PlayBackThread(self._command_message_queue)
        self._playback_thread.start()

    def stop(self):
        self._playback_thread.stop()
        self._playback_thread.join()
        self._playback_thread = None

    def _get_song_list(self, page, count, query=''):
        playlist = _get_play_list(self._playback_thread.playlist_pk)
        query = query or self._playback_thread.play_list_query
        song_list, total = playlist.get_paginated(page, count, query)
        return {'total': total,
                'song_list': [dict(in_playlist_pk=in_playlist_pk, **song.to_dict()) for song, in_playlist_pk in
                              song_list]
                }

    def _set_position(self, position):
        self._run_command('set_position', position)
        new_pos = self._playback_thread.update_and_get_position()
        return {'position': new_pos}

    def _register_commands(self, core):
        core.register_command('play_pause', 'Trigger play/pause base on current state.',
                              lambda: self._run_command('play_pause'))
        core.register_command('play_next', 'Play the next song.', lambda: self._run_command('play_next'))
        core.register_command('play_previous', 'Play the previous song.', lambda: self._run_command('play_previous'))
        core.register_command('play', 'Play the given song.', lambda pk: self._run_command('play', pk),
                              {'pk': 'pk inside the current playlist.'})
        core.register_command('stop', 'Stop playback', lambda: self._run_command('stop'))
        core.register_command('change_playlist', 'Switch playback to another playlist.',
                              lambda pk: self._run_command('change_playlist', pk),
                              {'pk': 'pk of the new  playlist.'})
        core.register_command('change_query', 'Change the playback query.',
                              lambda query: self._run_command('set_playlist_query', query),
                              {'query': 'A query string to filter the playlist used for playback.'})
        core.register_command('change_repeat_mode', 'Change the repeat mode of the playback.',
                              lambda new_mode: self._run_command('set_repeat_mode', new_mode),
                              {'new_mode': [RepeatOptions.ONE, RepeatOptions.NO, RepeatOptions.ALL]})  # TODO:
        core.register_command('get_status', 'Get information about the status of the player', self._get_status)
        core.register_command('enqueue', 'Enqueue a song to be played.', lambda pk: self._run_command('enqueue', pk),
                              {'pk': 'pk inside the current playlist.'})
        core.register_command('get_song_list', 'Get songs from the currently playing playlist.', self._get_song_list,
                              {'count': ParameterDescription('Number of results to return', 'count', int, 100),
                               'page': ParameterDescription('Page to return', 'page', int, 1),
                               'query': 'Use this query instead of the playlist one.'})
        core.register_command('set_position', 'Set the playback position',
                              self._set_position,
                              {'position': ParameterDescription(
                                  'New position as a float invalid values shall be ignored. ', 'position', float, 5)})
        core.register_command('set_volume', 'Set the volume.',
                              lambda volume: self._run_command('set_volume', volume),
                              {'volume': ParameterDescription(
                                  'New volume between 0 and 1, invalid values shall be ignored.', 'volume', float, 1)})

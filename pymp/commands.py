# coding=utf-8
class ValidationError(Exception):
    pass


class ParameterDescription(str):
    def __init__(self, description, name, validator, example):  # pylint: disable=W0231,W0613
        self.validator = validator
        self.example = example
        self.name = name

    def __new__(cls, value, *args, **kwargs):  # pylint: disable=W0613
        return super(ParameterDescription, cls).__new__(cls, value)

    def validate(self, value):
        try:
            self.validator(value)
        except Exception:
            raise ValidationError(
                "Failed validation expected to match {} got {} {}".format(self.validator.__name__, value,
                                                                          type(value).__name__))


class Command:

    def __init__(self, name, callback, description, params):
        self._name = name
        self._callback = callback
        self._description = description
        self._params = params or {}

    def __repr__(self):
        return self._name

    def as_dict(self):
        return {"name": self._name,
                "description": self._description,
                "parameters": self._params}

    def execute(self, parameters):
        for param, value in parameters.items():
            param_d = self._params.get(param)
            errors = []
            if not param_d:
                errors.append("Received Unknown parameter {}".format(param))
            if isinstance(param_d, ParameterDescription):
                try:
                    param_d.validate(value)
                except ValidationError as e:
                    errors.append(e)
            if errors:
                raise ValidationError(errors)
        return self._callback(**parameters)

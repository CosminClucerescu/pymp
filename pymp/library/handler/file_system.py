# coding=utf-8
import json
import os
import time
import traceback
from datetime import datetime
from multiprocessing import Queue, Process
from multiprocessing.dummy import Process as DummyProcess
from queue import Empty
from sqlalchemy import and_, not_
import taglib

from pymp import db
from pymp.library.handler.cueparser import CueSheet

MONITORED_DIRS = 'monitored_dirs'
LAST_RUN = 'last_run'

if "PYMP_TEST_IN_PROGRESS" in os.environ:
    # Just a stupid way to get coverage on this module while testing...
    Process = DummyProcess  # noqa
PROCESS_COUNT = 7


class AudioFileHandler:
    accepted_extensions = ['flac', 'wav', 'mp3', 'opus', 'ogg', 'ape', 'oga', 'mogg', 'aac', 'tta']

    @staticmethod
    def create_entry(file_path):
        song = taglib.File(file_path)
        try:
            track_num = int(song.tags["TRACKNUMBER"][0].split("/")[0]) if song.tags.get("TRACKNUMBER") else 0
        except ValueError:
            track_num = 0
        duration = song.length
        song = {"artist": " and ".join(song.tags.get('ARTIST', [])),
                "album": " and ".join(song.tags.get("ALBUM", [])),
                "title": " and ".join(song.tags.get("TITLE", [])),
                'track_number': track_num,
                "data_blob": "",
                }
        song['artist'] = song['artist'].strip() or 'unknown'
        song['title'] = song['title'].strip() or os.path.basename(file_path)
        song_wrapper = db.Song.get_or_create(**song)
        if not db.SongLink.get_one(song_id=song_wrapper.pk, link=file_path, raise_error=False):
            db.SongLink.create(song_id=song_wrapper.pk, link=file_path, handler='file_system', handler_info='{}',
                               duration=duration, __do_commit__=False)
        return 1


class CueFileHandler:
    accepted_extensions = ['cue']

    @staticmethod
    def create_entry(file_path: str):
        file_path_ext = file_path.split('.')[-1]
        if not file_path_ext == 'cue':
            return 0
        cue_sheet = CueSheet(file_path)
        last_track = len(cue_sheet.tracks)
        index = 0
        for index, track in enumerate(cue_sheet.tracks):
            song = {"artist": track.artist or cue_sheet.performer,
                    "album": cue_sheet.title,
                    "title": track.title,
                    'track_number': index + 1,
                    "data_blob": "",
                    }
            song_wrapper = db.Song.get_or_create(**song)
            track_start = track.offset_as_time_delta.total_seconds()
            track_end = track.offset_as_time_delta.total_seconds() + track.duration
            if index == last_track - 1:
                track_end = db.SongLink.TRACK_END
            if not db.SongLink.get_one(song_id=song_wrapper.pk, link=cue_sheet.file, start=track_start,
                                       duration=track.duration, raise_error=False):
                db.SongLink.create(song_id=song_wrapper.pk, link=cue_sheet.file, handler='file_system',
                                   handler_info='{}', duration=track.duration, start=track_start, end=track_end,
                                   __do_commit__=False)
        return index


FILE_HANDLERS = [AudioFileHandler(), CueFileHandler()]  # TODO: Shall be dynamically ...sometime


class ScannerProcess(Process):
    def __init__(self, queue, file_processor_queue, _id, last_scan):
        super(ScannerProcess, self).__init__()
        self._last_scan = last_scan or 0
        self._queue = queue
        self._file_processor_queue = file_processor_queue
        self._id = _id
        self.accepted_extension = CueFileHandler.accepted_extensions + AudioFileHandler.accepted_extensions
        self._start = time.mktime(datetime.now().timetuple())

    def _scan_dir(self, dir_path):
        if dir_path[-1] != '/':
            dir_path += '/'

        try:
            if os.stat(dir_path).st_atime == self._start:
                return
            os.utime(dir_path, (self._start, self._start))
            for entry in os.scandir(dir_path):
                if entry.is_dir():
                    self._queue.put((os.path.join(dir_path, entry.name), True))
                    continue
                file_path_ext = entry.name.split('.')[-1]
                if file_path_ext not in self.accepted_extension or entry.stat().st_atime <= self._last_scan:
                    continue
                file_path = os.path.join(dir_path, entry.name)
                os.utime(file_path, (self._start, entry.stat().st_mtime))
                self._file_processor_queue.put(file_path)

        except PermissionError:
            pass
        db.SESSION.session.commit()

    def run(self):
        while True:
            try:
                try:
                    dir_to_scan, _ = self._queue.get(True, 5)
                except Empty:
                    break
                self._scan_dir(dir_to_scan)
            except Exception:
                print(traceback.format_exc())
        self._file_processor_queue.put(None)
        config_info = db.ConfigInfo.get_one(name="lib_handler.{}".format('file_system'))
        data = json.loads(config_info.data_blob)
        data[LAST_RUN] = self._start
        config_info.update(data_blob=json.dumps(data))
        print("STOPPING SCANNER")


class FileProcessor(Process):
    def __init__(self, queue):
        super(FileProcessor, self).__init__()
        self._queue = queue

    def run(self):
        db.SESSION.engine.dispose()
        # start = time.time()
        count = 0
        while True:
            try:
                file_path = self._queue.get()
                # print(file_path)
                if file_path is None:
                    self._queue.put(None)
                    break

                for file_handler in FILE_HANDLERS:
                    try:
                        count += file_handler.create_entry(file_path)
                        if count >= 1000:
                            db.SESSION.session.commit()
                            count = 0
                    except OSError:
                        pass
                    except Exception:
                        pass
                        # print(traceback.format_exc())

            except Exception:
                pass
                # print(traceback.format_exc())
        print("STOPPING FILE PROCESSOR")
        db.SESSION.session.commit()
        # print(time.time() - start)


class FileSystemHandler:
    name = 'file_system'

    def __init__(self, core):
        """
        :type core: pymp.core.__main__._Core
        """
        self._register_commands(core)
        config_info = db.ConfigInfo.get_or_create(name="lib_handler.{}".format(self.name))
        self._data = json.loads(config_info.data_blob)
        self._monitored_dirs = []
        self.scanner_processes = []
        self.file_processors = []

    @staticmethod
    def _delete_removed_dirs(keep_dirs):
        query = [(not_(db.SongLink.link.like('{}%'.format(keep_dir)))) for keep_dir in keep_dirs]
        if len(query) > 1:
            query = and_(*query)
        else:
            query = query[0] if query else None
        try:
            db.SongLink.delete_by_query(query)
        except ValueError:
            pass

    def _update_data(self, key, value):
        self._data[key] = value
        config_info = db.ConfigInfo.get_one(name="lib_handler.{}".format(self.name))
        config_info.update(data_blob=json.dumps(self._data))

    def start_process(self, ignore_last_run=False):
        last_scan = self._data.get(LAST_RUN) if not ignore_last_run else 0
        queue = Queue()
        _file_processor_queue = Queue()
        self.scanner_processes = []
        self.file_processors = [FileProcessor(_file_processor_queue) for _ in range(PROCESS_COUNT)]
        self.scanner_processes.append(
            ScannerProcess(queue, _file_processor_queue, 0, last_scan=last_scan))
        self._monitored_dirs = self._data.get(MONITORED_DIRS, [])
        if not isinstance(self._monitored_dirs, list):
            self._monitored_dirs = [self._monitored_dirs]
        self._delete_removed_dirs(self._monitored_dirs)
        for monitored_dir in self._monitored_dirs:
            queue.put((monitored_dir, False))
        for proc in self.scanner_processes:
            proc.start()
        for proc in self.file_processors:
            proc.start()

    def rescan(self, ignore_last_run=False):
        if not ignore_last_run:
            config_info = db.ConfigInfo.get_or_create(name="lib_handler.{}".format(self.name))
            self._data = json.loads(config_info.data_blob)
        self.stop_process()
        self.start_process(ignore_last_run=ignore_last_run)

    def stop_process(self):
        try:
            for indx, proc in enumerate(self.scanner_processes):
                print("Joining scanner {}".format(indx))
                proc.join()

            for indx, proc in enumerate(self.file_processors):
                print("Joining processor {}".format(indx))
                proc.join()
        except (AssertionError, RuntimeError):
            pass

    def add_monitored_dir(self, path):
        if path not in self._monitored_dirs:
            self._monitored_dirs.append(path)
            self._update_data(MONITORED_DIRS, self._monitored_dirs)
            self.rescan(ignore_last_run=True)

    def remove_monitored_dir(self, path):
        try:
            self._monitored_dirs.remove(path)
            self._update_data(MONITORED_DIRS, self._monitored_dirs)
            self.rescan(ignore_last_run=True)
        except ValueError:
            pass

    def _register_commands(self, core):
        core.register_command('rescan', 'Trigger a rescan of the media library.', self.rescan)
        core.register_command('add_monitored_dir', 'Add a new directory to the list of monitored ones',
                              self.add_monitored_dir, {'path': 'Path of the directory to be added.'})
        core.register_command('remove_monitored_dir', 'Remove a directory from the list of monitored ones',
                              self.remove_monitored_dir, {'path': 'Path of the directory to be added.'})
        core.register_command('get_monitored_dirs', 'Get the list of monitored dirs', lambda: self._monitored_dirs)

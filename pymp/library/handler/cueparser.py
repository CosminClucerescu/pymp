import os
import re
from datetime import timedelta
from typing import List

import taglib

REM_REGEX = '^REM (.*)$'


class CueParserError(Exception):
    pass


class CueSheet:

    def __init__(self, path):
        self._no_more_lines = False
        self._path = path
        with open(path, 'rb') as file_p:
            self.data = file_p.read().decode(errors='ignore').split('\n')
        self.indx = -1

        self.rem = self._get_rem()
        self.performer = self.get_by_regex('^PERFORMER .(.*).$')
        self.song_writer = self.get_by_regex('^SONGWRITER .(.*).$')
        self.title = self.get_by_regex('^TITLE .(.*).$')
        self.file, self.aformat = self.get_by_regex('^FILE .(.*). (.*)$', True)
        self.file = os.path.join(os.path.dirname(self._path), self.file)
        self._target_duration = self._check_file_and_get_duration()
        self.tracks: List[CueTrack] = []
        self._get_tracks()

        if not self.tracks:
            raise CueParserError("Found no tracks")

    def _check_file_and_get_duration(self):
        # TODO: Assume that this might be wrongs sometime and try to match a file from that directory instead
        try:
            tags = taglib.File(self.file)
        except OSError:
            raise CueParserError("Target file not found.")
        return tags.length

    def _get_line(self):
        self.indx += 1
        try:
            return self.data[self.indx].strip()
        except IndexError:
            self._no_more_lines = True
            return None

    def get_by_regex(self, regex, must_find=False):
        line = self._get_line()
        while not line:
            line = self._get_line()
            if self._no_more_lines:
                raise CueParserError("Reached end and did not find {}".format(regex))

        match = re.match(regex, line)
        if match:
            groups = match.groups()
            if len(groups) > 1:
                return groups
            return groups[0]
        if must_find:
            return self.get_by_regex(regex, must_find)
        if self.indx:
            self.indx -= 1

    def _get_rem(self):
        rem_tmp = []
        try:
            data = self.get_by_regex(REM_REGEX, must_find=True)
        except CueParserError:
            self.indx = -1
            return ''
        while data:
            rem_tmp.append(data)
            data = self.get_by_regex(REM_REGEX)

        return "\n".join(rem_tmp)

    def _get_tracks(self):
        while True:
            try:
                line = self.get_by_regex('^(TRACK.*$)', must_find=True)
            except CueParserError:
                break
            if not line:
                continue
            cuetrack = CueTrack(self)
            self.tracks.append(cuetrack)
        self.tracks[-1].duration = (timedelta(seconds=self._target_duration) - self.tracks[
            -1].offset_as_time_delta).total_seconds()
        if self.tracks[-1].duration <= 0:
            raise CueParserError("This can't be right: Last track has zero or negative duration?")


class CueTrack:
    def __init__(self, parser):
        self.artist = parser.get_by_regex('^PERFORMER .(.*).$')
        self.songwriter = parser.get_by_regex('^SONGWRITER .(.*).$')
        self.title = parser.get_by_regex('^TITLE .(.*).$', must_find=True)
        self.flags = parser.get_by_regex('^FLAGS .(.*).$')
        self.isrc = parser.get_by_regex('^ISRC .(.*).$')
        self.index, self.offset = parser.get_by_regex('^INDEX (.*) (.*)$', must_find=True)
        self.duration = None
        if parser.tracks:
            previous = parser.tracks[-1]
            previous.duration = (self.offset_as_time_delta - previous.offset_as_time_delta).total_seconds()

    @property
    def offset_as_time_delta(self):
        if not self.offset:
            raise CueParserError("Unexpected empty offset")
        offset = self.offset.split(':')
        if len(offset) == 1:
            offset = timedelta(minutes=int(offset[0]))
        elif len(offset) == 2:
            offset = timedelta(minutes=int(offset[0]), seconds=int(offset[1]))
        elif len(offset) == 3:
            if len(offset[2]) < 3:
                offset[2] += "0"
            offset = timedelta(minutes=int(offset[0]), seconds=int(offset[1]), milliseconds=int(offset[2]))
        else:
            raise CueParserError("Wrong offset value")
        return offset
